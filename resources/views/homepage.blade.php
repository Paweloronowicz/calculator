<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">

    <title>Calculator App</title>
  </head>
  <body>
    <div class="col-sm-6 offset-md-3">
      <h1 class="text-center my-5">PHP Calculator App</h1>

      <form id="calculatorForm" onsubmit="submitCalculation(event)" method="POST">
        @csrf
        <div class="mb-3">
          <label for="value1" class="form-label">Value #1</label>
          <input type="text" class="form-control calculator-value" id="value1" name="value1" aria-describedby="value1Help" autocomplete="off">
          <div id="value1Help" class="form-text">Must be a valid number</div>
        </div>
        <div class="mb-3">
          <label for="calculationType" class="form-label">Calculation type</label>
          <select class="form-select" id="calculationTypeId" name="calculationTypeId" aria-label="Default select example" required>
            <option value="" selected>-- Select --</option>
            @foreach($calculationTypes as $calculationType)
              <option value="{{ $calculationType->id }}">{{ $calculationType->name }}</option>
            @endforeach
          </select>
        </div>
        <div class="mb-3">
          <label for="value2" class="form-label">Value #2</label>
          <input type="text" class="form-control calculator-value" id="value2" name="value2" aria-describedby="value2Help" autocomplete="off">
          <div id="value2Help" class="form-text">Must be a valid number</div>
        </div>
        <button type="submit" class="btn btn-primary">Calculate</button>
      </form>

      <div class="alert alert-danger my-4" id="calculatorFormError" role="alert">
      </div>

      <h1 class="text-center my-5" id="calculatorResult"></h1>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="{{ asset('js/custom.js') }}"></script>
  </body>
</html>