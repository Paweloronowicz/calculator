<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCalculationTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calculation_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('values_required');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });

        // insert calculation types
        DB::table('calculation_types')->insert([
          ['name' => 'Addition', 'values_required' => 2],
          ['name' => 'Subtraction', 'values_required' => 2],
          ['name' => 'Multiplication', 'values_required' => 2],
          ['name' => 'Division', 'values_required' => 2],
          ['name' => 'Square root', 'values_required' => 1],
          ['name' => 'Exponential expression', 'values_required' => 2]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calculation_types');
    }
}
