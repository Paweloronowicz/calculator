$(document).ready(function() {
  // declare form inputs and other values
  var value1;
  var value2;
  var calculationTypeId;
  var calculationTypes = [];

  // only allow float numbers
  $('.calculator-value').keypress(function(event) {
    if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
      event.preventDefault();
    }
  });

  getCalculationTypes = function() {
    $.getJSON("getCalculationTypes", function(data) {
      $.each(data, function(index, item) {
        calculationTypes.push(item);
      });
    });
  }
  getCalculationTypes();

  submitCalculation = function(event) {
    event.preventDefault();

    // assign form values
    value1 = $("#value1").val();
    value2 = $("#value2").val();
    calculationTypeId = $("#calculationTypeId").find(":selected").val();

    var formValid = validateCalculation();
    if(formValid === true) {
      $.ajax({
        type: "POST",
        dataType: "json",
        url: "calculate",
        data: { 
          _token: $('meta[name="csrf-token"]').attr('content'),
          value1: value1,
          value2: value2,
          calculationTypeId: calculationTypeId
        }
      }).done(function(result) {
        $("#calculatorResult").html(result);
      }).fail(function() {
        $("#calculatorResult").html("Something went wrong. Your entered values might be too big or invalid");
      });
    }
  }

  validateCalculation = function() {
    // reset error alert and result
    $("#calculatorResult").html("")
    $("#calculatorFormError").html("").css("display", "none");
    var errorMessage = "";

    // check if enough values were entered by user
    var valuesRequired;
    $.each(calculationTypes, function(index, item) {
      if(item.id == calculationTypeId) {
        valuesRequired = item.values_required;
      }
    });
    if(valuesRequired == 2) {
      if(value1 == "" || value2 == "" || calculationTypeId == 0 || calculationTypeId == "") {
        errorMessage = "All values must be entered";
      }
    }
    if(valuesRequired == 1) {
      if((value1 == "" && value2 == "") || calculationTypeId == 0 || calculationTypeId == "") {
        errorMessage = "One of the values must be entered";
      }
      if((value1 != "" && value2 != "") || calculationTypeId == 0 || calculationTypeId == "") {
        errorMessage = "Only one of the values can be entered";
      }
    }

    // check if inputs are numbers
    if(isNaN(value1) || isNaN(value2)) {
      errorMessage = "Values must be valid numbers";
    }
    
    if(errorMessage != "") {
      $("#calculatorFormError").html(errorMessage).css("display", "block");
      return false;
    }

    return true;
  }
});