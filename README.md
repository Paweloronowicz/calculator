## About

This PHP Calculator App allows the user to make several different mathematical calculations. It uses the Laravel framework and Bootstrap.

## Installation

After cloning the repository, run the following commands from the root of the project:

`php artisan mysql:createdb calculator` - creates the schema
`php artisan migrate` - generates the DB tables
`composer update` - updates project's dependencies

Then edit the `.env` file to modify the config, mainly your local database connection and whether to use the debug mode. These are the values I use in my local environment:

`APP_NAME=Calculator
APP_ENV=local
APP_KEY=base64:Z0NuwrWtL2GkMMwmao35HN0HF5rIvRAYkUNmNdmGi8I=
APP_DEBUG=true
APP_URL=http://calculator.local`

`DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=calculator
DB_USERNAME=root
DB_PASSWORD=`

## Scheduled job

There is a scheduled job that deletes logs older than 5 days. You can run it in the command line with `php artisan schedule:run`. It's scheduled to run every minute to allow easier testing in a local Windows environment but on a live server it could be changed to run e.g. hourly.

## Getting latest logs

To see calculation logs from the last 24 hours in JSON, go to `/get_latest_logs`, e.g. http://calculator.local/get_latest_logs