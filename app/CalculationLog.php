<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CalculationLog extends Model
{
  protected $table = 'calculation_log';
}
