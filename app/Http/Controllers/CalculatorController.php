<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;
use \App\CalculationType;
use \App\CalculationLog;

class CalculatorController extends Controller
{
  public function view() {
    $calculationTypes = CalculationType::all();
    
    return view('homepage')->with('calculationTypes', $calculationTypes);
  }

  public function getCalculationTypes() {
    $calculationTypes = CalculationType::all();

    $calculationTypes = $calculationTypes->toJson();
    print $calculationTypes;
  }

  public function calculate(Request $request) {
    $data = $request->all();
    $value1 = $data['value1'];
    $value2 = $data['value2'];
    $calculationTypeId = $data['calculationTypeId'];

    if(!is_numeric($value1) || !is_numeric($value2) || !is_numeric($calculationTypeId)) {
      print "Your entered values must be numbers";
      return false;
    }
    
    switch($calculationTypeId) {
      case 0:
        $result = "Something went wrong. Try smaller numbers";
        break;

      // addition
      case 1:
        $result = round($value1 + $value2, 1);
        break;

      // subtraction
      case 2:
        $result = round($value1 - $value2, 1);
        break;

      // multiplication
      case 3:
        $result = round($value1 * $value2, 1);
        break;

      // division
      case 4:
        $result = round($value1 / $value2, 3);
        break;

      // square root
      case 5:
        if(!$value1) {
          $result = round(sqrt($value2), 3);
        } else {
          $result = round(sqrt($value1), 3);
        }
        break;

      // exponential expression
      case 6:
        $result = round(pow($value1, $value2), 3);
        break;
    }
    $this->storeResult($request);
    print $result;
  }

  public function storeResult(Request $request) {
    $calculationLog = new CalculationLog;

    $calculationLog->value1 = $request->value1;
    $calculationLog->value2 = $request->value2;
    $calculationLog->calculation_type_id = $request->calculationTypeId;

    $calculationLog->save();
  }
}
