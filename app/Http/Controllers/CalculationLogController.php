<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\CalculationLog;

class CalculationLogController extends Controller
{
  public function getLatestLogs() {
    $dayAgo = date('Y-m-d H:i:s', strtotime("-1 days"));
    $calculationLogs = CalculationLog::where('created_at', '>=', $dayAgo)->orderBy('created_at', 'desc')->get();
    print $calculationLogs->toJson();
  }
}
